Usage:
* Install & enable the module.
* Goto: admin/config/search/url_alias_restriction and set the path element, 
  which should make a URL inaccessible.
* Edit a node and set the url alias to eg: restricted-access/{name-of-node}.
* Try to access the node as an anonymous user.

Also works for other Drupal paths which can be aliased, eg. taxonomy pages.
